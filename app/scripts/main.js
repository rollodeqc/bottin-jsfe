(function() {
  /*global Isotope JST imagesLoaded _ CountUp slug Locally*/
  'use strict';

  var perPage = 12;
  var store = new Locally.Store({ compress: true });
  var users;

  var avecRepertoire = function () {
    return !!document.querySelector('.avec-repertoire');
  };

  var ficheTeaserDom = function (user) {
    var div = document.createElement('div');
    var picks = store.get('picks') || {};

    if (user.blog && user.blog.indexOf('http://') && user.blog.indexOf('https://')) {
      user.blog = 'http' + (
        user.blog.indexOf('htp://') ?
          ('://' + user.blog) :
          user.blog.slice(3)
        );
    }

    div.innerHTML = JST.ficheTeaser({user: user});
    div.setAttribute('data-id', user.id);
    div.setAttribute('data-login', user.login);
    if (picks[user.login]) { div.classList.add('pick'); }
    if (user.location.city) { div.setAttribute('data-city', user.location.city); }
    return div;
  };


  var withKeyword = function(user, keyword) {
    var r;
    var t;
    var kws;
    if (!user.keywords || !user.keywords.length) { return false; }
    kws = keyword.split(' ');
    for (t = 0; t < kws.length; ++t) {
      for (r = 0; r < user.keywords.length; ++r) {
        if (user.keywords.indexOf(kws[t]) !== -1) { return true; }
      }
    }
    return false;
  };

  var programsIn = function(user, langage) {
    var r;
    if (typeof user.public_repos !== 'object') { return false; }

    for (r in user.public_repos) {
      if (user.public_repos[r].language && user.public_repos[r].language.toLowerCase() === langage) { return true; }
    }
    return false;
  };

  var clickSection = function(event) {
    var target = event.target;
    var picks = store.get('picks') || {};
    var login, id;

    if (target.tagName === 'A' && target.getAttribute('href') && target.getAttribute('href').slice(0, 1) !== '#') { return; }

    login = target.getAttribute('data-login');
    while (!login) {
      target = target.parentNode;
      login = target.getAttribute('data-login');
      id = target.getAttribute('data-id');
    }
    target.classList.add('picking');
    if (target.classList.contains('pick')) { delete picks[login]; }
    else { picks[login] = id; }
    target.classList.toggle('pick');

    store.set('picks', picks);
    setTimeout(function() {
      target.classList.remove('picking');
    }, 400);
  };

  var displayUsers = function() {
    var body = document.getElementsByTagName('body')[0];
    var nFichesEl = document.getElementById('compteFiches');

    var nPagesEl = document.getElementsByClassName('nPages');
    var currentPageEl = document.getElementsByClassName('currentPage');


    var sectionFiches = document.createElement('section');
    var villeInput = slug(document.getElementsByName('ville')[0].value || '');
    var langageInput = document.getElementsByName('langage')[0].value.toLowerCase();
    var keywordInput = document.getElementsByName('keyword')[0].value.toLowerCase();
    var hireableInput = document.getElementsByName('hireable')[0].checked;
    var webInput = document.getElementsByName('web')[0].checked;
    var cieInput = document.getElementsByName('cie')[0].checked;
    var emailInput = document.getElementsByName('email')[0].checked;
    var sortInput = document.getElementsByName('sort')[0].value;
    var parts = villeInput.split('-');
    var touched = false;
    var wasN = parseInt(nFichesEl.innerHTML, 10) || 0;
    var picks = store.get('picks');
    var filteredUsers, sample, numAnim, page, r, nPages, section, iso;
    if (sortInput !== 'mes choix') {
      if (parts[0] === 'st') {
        parts[0] = 'saint';
        touched = true;
      } else if (parts[0] === 'ste') {
        parts[0] = 'sainte';
        touched = true;
      }
      if (touched) { villeInput = parts.join('-'); }
      filteredUsers = users.filter(function (user) {
        return (!villeInput || slug(user.location.city || '') === villeInput) &&


          (!keywordInput || withKeyword(user, keywordInput)) &&



          (!langageInput || programsIn(user, langageInput)) &&
          (!hireableInput || user.hireable) &&
          (!webInput || user.blog) &&
          (!cieInput || user.company) &&
          (!emailInput || user.email);
      });
    }

    switch (sortInput) {
    case 'mes choix':
      if (picks) {
        filteredUsers = Object.keys(picks);
        sample = users.filter(function (user) {
          return filteredUsers.indexOf(user.login) !== -1;
        });
      } else {
        filteredUsers = [];
        sample = [];
      }
      break;

    case '# de followers':
    case 'followers':
      sample = _(filteredUsers)
        .sortBy(function(user) { return user.followers || 0; })
        .reverse()
        .value();
      break;

    case '# de projets':
    case 'projets':
    case 'repos':
      // ATTENTION: il n'y a que 30 repos par user dans le json
      //            quand il devrait y en avoir beaucoup plus
      sample = _(filteredUsers)
        .sortBy(function(user) {
          var projets = 0;

          if (typeof user.public_repos === 'object') {
            user.public_repos.forEach(function (repo) {
              if (repo.language) { ++projets; }
            });
          }
          return projets;
        })
        .reverse()
        .value();
      break;

    case 'github':
    case 'sur github depuis':
    case 'since':
      sample = _(filteredUsers)
        .sortBy(function(user) { return Date.parse(user.created_at); })
        .value();
      break;

    default:
      sample = _.shuffle(filteredUsers);
      break;
    }

    page = parseInt(currentPageEl[0].innerHTML, 10) - 1;
    nPages = Math.ceil(sample.length / perPage) - 1;
    if (page > nPages) {
      page = nPages;
      for (r = 0; r < currentPageEl.length; ++r) {
        currentPageEl[r].innerHTML = page + 1;
      }
    }

    ++nPages;
    for (r = 0; r < nPagesEl.length; ++r) { nPagesEl[r].innerHTML = nPages; }

    sample = sample.slice(page * perPage, page * perPage + perPage);
    nFichesEl.innerHTML = filteredUsers.length;
    if (wasN !== filteredUsers.length) {
      numAnim = new CountUp(
        nFichesEl, wasN, filteredUsers.length, 0, 1 + Math.abs(wasN - filteredUsers.length) / 1500, {
          useEasing: true,
          useGrouping: false
        }
      );
      numAnim.start();
    }
    sample.forEach(function (user) { sectionFiches.appendChild(ficheTeaserDom(user)); });
    section = document.getElementsByTagName('section')[0];
    section.removeEventListener('click', clickSection);
    body.replaceChild(sectionFiches, section);
    sectionFiches.addEventListener('click', clickSection);
    // otherwise the grid is all screwed up
    iso = new Isotope(sectionFiches);
    imagesLoaded(sectionFiches, function() {
      iso.arrange();
    });
  };

  var setupPageRepertoire = function() {
    var inputs = document.querySelectorAll('.buttons input');
    var sortInput = document.getElementsByName('sort')[0];
    var r;

    for (r = 0; r < inputs.length; ++r) { inputs[r].addEventListener('change', displayUsers); }
    sortInput.addEventListener('change', displayUsers);
  };

  var pagerClick = function(event) {
    var direction = event.target.value;
    var nPagesEl = document.getElementsByClassName('nPages');
    var currentPageEl = document.getElementsByClassName('currentPage');
    var currentPage = parseInt(currentPageEl[0].innerHTML, 10);
    var nPages = parseInt(nPagesEl[0].innerHTML, 10);
    var r;

    if (direction === 'previous') {
      if (--currentPage < 1) { currentPage = nPages; }
    } else if (direction === 'next') {
      if (++currentPage > nPages) { currentPage = 1; }
    } else { return; }

    for (r = 0; r < currentPageEl.length; ++r) { currentPageEl[r].innerHTML = currentPage; }
    displayUsers();
  };

  var setupPager = function() {
    var buttonsEl = document.querySelectorAll('.pager > button');
    var r;

    for (r = 0; r < buttonsEl.length; ++r) { buttonsEl[r].addEventListener('click', pagerClick); }
  };

  var getUsers = function(cb) {
    var nFichesEl = document.getElementById('compteFiches');

    store.remove('users-8000-a');
    nFichesEl.classList.add('growing');
    fetch('/rollodeqc-8000-c.json')
      .then(function (response) { return response.json(); })
      .then(function(data) {
        var spammers = [
          'JeyInkl',
          'Piston-Drill-Wrong-Hole',
          'Doumlev',
          'fuitevaginal',
          'tabla1122'
        ];

        users = data.filter(function (user) {
          return user.location &&
            user.location.province === 'quebec' &&
            user.type === 'User' && //Organization
            spammers.indexOf(user.login) === -1;
        });
        cb();
        setupPageRepertoire();
        nFichesEl.classList.remove('growing');
      });
  };

  window.store = store;
  if (avecRepertoire()) {
    getUsers(displayUsers);
    setupPager();
  }
}());

